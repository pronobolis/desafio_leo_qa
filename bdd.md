	Funcionalidade : Log in
		Eu como usuário administrador 
		Quero efetuar log in em "Moodle sandbox demo"
		Para ter acesso aos meus cursos 

	Cenário de teste 1 : Acessar sistema como "Administrador".

		Dado que estou na página log in
		Quando informo meu usuário "admin"
			E minha senha "sandbox"
			E não marco a opção "Lembrar indentificação de usuário"
		Então devo ser redirecionado para página Home
			E ao redefinir o acesso ou efetuar log out devo ser redirecionado para página log in



	Funcionalidade : Criar curso
		Eu como administrador 
		Quero criar um curso fake 
		Para ter acesso ao curso criado
		
	Cenário de teste 2: Criar curso "Fake" 
		
		Dado que estou em lar/Administração do site/Cursos/Gerenciar cursos e categorias/Adicionar um novo curso
		Quando Crio um curso 
		Então sou redirecionado para página do curso "fake"