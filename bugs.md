	1� BUG
	Passos para reproduzir a situa��o:
		Navega��o por entre as p�ginas 
		"Menu lateral" e "Caminho superior" desconexos 
		Navegar por entre os mesmo nomes de p�ginas, leva a p�ginas com layouts distintos.
		
		Exemplo p�gina cursos:
		"Menu lateral"
		<a href="https://imgbox.com/k5PnbR0F" target="_blank"><img src="https://thumbs2.imgbox.com/39/ec/k5PnbR0F_t.png" alt="image host"/></a>
		"Caminho superior" 
		<a href="https://imgbox.com/G2up7de6" target="_blank"><img src="https://thumbs2.imgbox.com/c4/65/G2up7de6_t.png" alt="image host"/></a>
		<a href="https://imgbox.com/xQKz6367" target="_blank"><img src="https://thumbs2.imgbox.com/83/2f/xQKz6367_t.png" alt="image host"/></a>

	Resultado atual:
		"Menu lateral" e "Caminho superior" Direcionados para paginas com mesmas funcionalidades, por�m com interfaces diferentes. 

	Resultado esperado:
		"Menu lateral" e "Caminho superior" Devem levar as mesmas p�ginas, com a melhor interface e mantendo o padr�o entre todas. 

			Obs: "Menu lateral" est� em todos os casos validados, levando ao tipo de p�gina mais adequada. 


		2� BUG
		Passos para reproduzir a situa��o:
			Menu lateral op��o "Meus cursos", bot�o aparece somente depois de criar e navegar por algum curso.
			Bot�o est� desativado e n�o apresenta nenhum retorno aparente que o diferencie dos demais bot�es do menu lateral
			<a href="https://imgbox.com/r15QNmoR" target="_blank"><img src="https://thumbs2.imgbox.com/60/e2/r15QNmoR_t.png" alt="image host"/></a>

		Resultado atual:
			Bot�o n�o possui funcionalidade atribuida 
			N�o existe um sinal visual de seu bloqueio 

		Resultado esperado:
			Adicionar sinaliza��o vizual que bot�o est� desabilitado 
			ou 
			Ativar funcionalidade do bot�o.
		
		
			3� BUG
			Passos para reproduzir a situa��o:


			Resultado atual:


			Resultado esperado:

