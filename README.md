		https://bitbucket.org/leolearningbrasil/desafio-leo-qa/src/master/
		Desafio LEO - Analista de teste
		Orientações
				• Para o desenvolvimento do projeto deverá ser criado um repositório na conta do bitbucket ou github do candidato com o seguinte nome "desafio_leo_qa"(ex.:  http://bitbucket.org/seu_nome/desafio_leo_qa).
					○ Criado arquivos separados para caso, bdd, bugs
				• Ao finalizar, enviar e-mail para plataforma@leolearning.com com a url do seu repositório, com o assunto [Desafio LEO] - Finalizado.
				• Em caso de dúvidas, enviar e-mail para plataforma@leolearning.com, com o assunto [Desafio LEO] - Dúvidas.
				• Fique a vontade para adicionar qualquer tipo de conteúdo que julgue útil ao projeto.
			Obs.: Não fazer um Pull Request para este projeto!
		
		Sobre o desafio
		
			A seguir temos um desafio para entender um pouco melhor sobre seu conhecimento. Para resolve-lo você deverá utilizar o site  https://sandbox.moodledemo.net/login/index.php
				1. Desenvolva os casos de testes a partir das histórias apresentadas abaixo, descrevendo ao menos um cenário feliz e um cenário alternativo para cada uma das histórias.
				2. Descreva os bugs e os cenários para encontrá-los. Evidencie com prints.
				3. Escreva o BDD de ao menos um casos de teste.

				4. Faça a automação do cadastro do curso:
					○ Deverá utilizar um framework de testes a sua escolha, desde que não seja um framework de reprodução/gravação.
					○ Uso de Page Object
				
		Diferenciais/Extras
			• Automação
			• BDD
			• Uso de Behat
			• Uso da linguagem Javascript
			
		Histórias:
			1. Como o "Administrador" gostaria de realizar o login:
				• Para realizar o login serão necessários os seguinte campos: usuário e senha;
				• A página deverá utilizar as cores branco, azul e cinza e todos os campos obrigatórios precisam ser sinalizados com um asterisco vermelho;
			 Deve ser possível aceitar ou não a opção "Lembrar identificação de usuário" e isso deverá ser feito através de um checkbox.
			3. Como o "Administrador" gostaria de criar um curso fake:
				• No menu "Administração do site", gostaria de ter a aba "Cursos" com opção para adicionar um novo curso;
				• No cadastro de curso, os campos obrigatórios devem estar marcados;
				• O campo "Número de identificação do curso" deve aceitar apenas números;
			Ao preencher com informações fake e salvar, gostaria de ser redirecionado para o curso.