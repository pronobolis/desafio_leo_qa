			Funcionalidade 1 : Log in
				Link: https://sandbox.moodledemo.net/login/index.php
					Usuário: admin
					Senha: sandbox
				Check box "Lembrar identificação do usuário": SIM/NÃO


			Cenário de teste 1 : Acessar sistema como "Administrador".

				Ação 1.1:
					Acessar com dados CORRETOS 
					Check box "lembrar" NÃO
					Clique em acessar 

				Resultado esperado 1.1:
					Deve acessar em página Home
					E ao redefinir o acesso
					Ou efetuar LOG OUT devo ser redirecionado para página LOG IN
					E os dados não devem estar preenchidos até que reinicie o acesso.


				Ação 1.2:
					Acessar com dados CORRETOS 
					Check box "lembrar" SIM
					Clique em acessar 

				Resultado esperado 1.2:
					Deve acessar em página Home
					E ao redefinir o acesso Devo seguir acessado com meu usuário em página Home
					Ou efetuar LOG OUT devo ser redirecionado para página LOG IN
					E os dados devem estar preenchidos nos campos pronto para acessar.


				Ação 1.3:
					Acessar com dados ERRADOS
					Check box "lembrar" SIM/NÃO
					Clique em acessar 

				Resultado esperado 1.3:
					Deve receber mensagem de alerta acima do botão acessar 
					E informar que, usuário ou senha estão incorretos. 
					Campo usuário deve ser mantido preenchido com dado informado
					Campo senha deve ser limpo
						Para Check box SIM: Dado não deve ser armazenado até resultar em um acesso valido.
											Chack box deve ser mantida marcada 


			Funcionalidade 2 : Cadastro do curso
				Página: lar/Administração do site/Cursos/Gerenciar cursos e categorias/Adicionar um novo curso
					Usuário: admin
					Senha: sandbox

				Curso "Fake"

			Cenário de teste 2 : Curso "Fake"

				Ação 2.1:
					Cadastrar curso 
					Completando "somente" campos "obrigatórios"
					Clico em "Salvar e exibir"

				Resultado esperado 2.1:
					Curso deve ser salvo com "sucesso"
					Página deve ser redirecionada 
						De "Adicionar um novo curso" 
						Para "Cadastro Curso Fake"


				Ação 2.2:
					Cadastrar curso 
					Completando "Todos" os campos
					Clico em "Salvar e exibir"

				Resultado esperado 2.2:
					Curso deve ser salvo com "sucesso"
					Página deve ser redirecionada 
						De "Adicionar um novo curso" 
						Para "Cadastro Curso Fake"


				Ação 2.3:
					Cadastrar curso 
					Completando "somente" os campos
						"Nome completo do curso"
						E "Resumo do curso"
					Clico em "Salvar e exibir"

				Resultado esperado 2.3:
					Curso "NÃO" deve ser salvo com "sucesso"
					Página deve ser direcionado ao campo obrigatio faltante
						Fixada no campo mais ao topo
					Todos os campos obrigatorios devem estar destacados em vermelho
						Com as mensagens de alertas correspondentes a cada campo 


				Ação 2.4:
					Cadastrar curso 
					Completando "somente" o campo
						"Número de ID do curso"
						E inserir dados "alfanúmericos" 
						E passo com o enter

				Resultado esperado 2.4:
					Campo deve remover automaticamente 
						letras 
						e carácteres 
						que forem inseridos 
					Mantendo apenas os número inclusos 



