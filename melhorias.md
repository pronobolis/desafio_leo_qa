    Passos para reproduzir a situação:
        Menu lateral ao ser selecionado opção que abra varias opções ainda no menu lateral 
        é duplicada opção selecionada 
        Ficando vizualmente desconexo 
            <a href="https://imgbox.com/scwcbEid" target="_blank"><img src="https://thumbs2.imgbox.com/2b/af/scwcbEid_t.png" alt="image host"/></a>

    Resultado atual:
        Menu lateral se divide em duas partes ao abrir mais opções no proprio menu lateral 
    Resultado esperado:
        Menu lateral deve funcionar como um sistema de raizes de pastas padrão 
            Onde se selecionada uma opção, as opções filhas da opção selecionada, ficarão abertas em sequencia a opção relacionada e a baixo dela. 
            Sem deslocar-se das demais opção do menu lateral. 
